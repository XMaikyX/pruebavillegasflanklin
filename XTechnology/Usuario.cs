﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XTechnology
{
    class Usuario:Cliente
    {
        private string nuCedula;

        public string NuCedula
        {
            get { return nuCedula; }
            set { nuCedula = value; }
        }
        public Usuario(string nombres, string apellido, string Nucedula)
        {
            this.Unombres = nombres;
            this.Uapellido = apellido;
            this.NuCedula = Nucedula;
           
        }
        private string unombres;

        public string Unombres
        {
            get { return unombres; }
            set { unombres = value; }
        }
        private string uapellido;

        public string Uapellido
        {
            get { return uapellido; }
            set { uapellido = value; }
        }
        private List<Cliente> listacliente;

        public List<Cliente> Listacliente
        {
            get { return listacliente; }
            set { listacliente = value; }
        }
        //public string ImprimirDatosPersonales()
        //{
        //    return this.Unombres + " " + this.Uapellido + " " +
        //           this.NuCedula+"    "+this.Listacliente;
        //}

        //agrega clientes 
        public void AgregarCliente(Cliente nuevocliente)
        {
            this.listacliente.Add(nuevocliente);
        }
        public void QuitarCliente(Cliente cliente)
        {
            this.listacliente.Remove(cliente);
        }

    }
}
