﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XTechnology
{
    class Audifono:Dispositivo
    {
        private string alambrico;

        public string Alambrico
        {
            get { return alambrico; }
            set { alambrico = value; }
        }
        private string inalambrico;

        public string Inalambrico
        {
            get { return inalambrico; }
            set { inalambrico = value; }
        }

    }
}
